module.exports = {
  port: process.env.PORT || 3000,
  isDebug: process.env.DEBUG && process.env.DEBUG.toLowerCase() === "true",
  trustProxy: process.env.TRUST_PROXY || "loopback",
  mastodonToken: process.env.MASTODON_TOKEN,
  mastondonApi: process.env.MASTODON_API_URL || "https://mamot.fr/api/v1/",
};
