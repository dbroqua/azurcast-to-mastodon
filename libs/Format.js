import fs from "fs";
import request from "request";
import Masto from "mastodon";
import { mastodonToken, mastondonApi, isDebug } from "../config";

/**
 * Fonction permettant de formater une réponse API
 * @param  {Object} req
 * @param  {Object} res
 * @param  {Functiont} next
 * @param  {Object} err
 * @param  {Object} response {code: Integer, res: Array/Object}
 */
const _formatResponse = (req, res, next, err, response) => {
  if (err) {
    req.response = response;
    next(err);
  } else {
    switch (req.method) {
      case "GET":
        res
          .status(response ? 200 : 204)
          .json(response)
          .end();
        break;
      case "PATCH":
        res.status(200).json(response).end();
        break;
      case "DELETE":
        res.status(200).json(response).end();
        break;
      case "POST":
        res.status(201).json(response).end();
        break;
      default:
        next(new Error("Not implemented"));
        break;
    }
  }
};

/**
 * Fonction permettant de formater une erreur
 * @param  {Object} res
 * @param  {Object} err
 */
const _formatResponseError = (res, err) => {
  const code = err.errorCode || 500;
  const response = {
    code,
    message: err.message,
  };

  res.status(Math.trunc(code)).json(response);
};

/**
 * Fonction permettant de télécharger la pochette d'un album selon une URL donnée
 * @param  {String} coverUrl
 * @param  {Function} callback
 */
const _getMedia = (coverUrl, callback) => {
  const dest = "/tmp/attachment.jpg";
  const file = fs.createWriteStream(dest);

  try {
    request({
      uri: coverUrl.replace("localhost", "azura.shoutca.st"),
      headers: {
        "Cache-Control": "max-age=0",
        Connection: "keep-alive",
        "User-Agent":
          "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
      },
    })
      .on("error", (error) => {
        callback(error);
      })
      .pipe(file)
      .on("finish", () => {
        callback(null, dest);
      });
  } catch (error) {
    callback(error);
  }
};

/**
 * Fonction formattant le texte à publier
 * @param  {Object} values
 */
const _formatMessage = (values) => {
  return `#rx3 #nowplaying ${values.artist} - ${values.title}`;
};

/**
 * Fonction publiant un message (et média si attaché) sur Mastdon
 * @param {Object} playing
 * @param {Object} logger
 * @param {Function} cb
 */
const _publishMessage = (playing, logger, cb) => {
  const callback = (err, res) => {
    if (err) {
      cb(err);
    } else {
      cb(null, { id: res.id });
    }
  };

  if (playing.song) {
    const status = _formatMessage(playing.song);
    const cover = playing.song.art;

    // Instanciation de Mastodon
    const M = new Masto({
      access_token: mastodonToken,
      api_url: mastondonApi,
    });

    if (cover) {
      _getMedia(cover, (err, dest) => {
        if (isDebug) {
          logger.info({ cover, status });
        } else if (err) {
          M.post("statuses", { status }, callback);
        } else {
          M.post("media", { file: fs.createReadStream(dest) })
            .then((resp) => {
              const { id } = resp.data;
              M.post("statuses", { status, media_ids: [id] }, callback);
            })
            .catch(() => {
              M.post("statuses", { status }, callback);
            });
        }
      });
    } else if (isDebug) {
      logger.info(status);
    } else {
      M.post("statuses", { status }, callback);
    }
  } else {
    callback(new Error("Missing song object"));
  }
};

export const formatResponse = _formatResponse;
export const formatResponseError = _formatResponseError;
export const getMedia = _getMedia;
export const formatMessage = _formatMessage;
export const publishMessage = _publishMessage;
