module.exports = {
  parser: "babel-eslint",
  extends: ["airbnb-base", "prettier", "plugin:jest/recommended"],
  plugins: ["prettier", "jest"],
  env: {
    es6: true,
    browser: false,
    node: true,
    "jest/globals": true
  },
  globals: {
    __DEV__: true
  },
  rules: {
    "no-plusplus": [2, { allowForLoopAfterthoughts: true }],
    "no-underscore-dangle": "off",
    indent: ["error", 2, { SwitchCase: 1 }],
    "linebreak-style": ["error", "unix"],
    quotes: ["error", "double"],
    semi: ["error", "always"],
    "import/no-extraneous-dependencies": ["error", { packageDir: "." }],
    "prettier/prettier": "error",
    "func-names": ["error", "never"],
    "no-restricted-globals": ["error", "event", "fdescribe"]
  }
};