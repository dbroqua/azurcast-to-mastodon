# azurcast-to-mastodon

Simple plugin permettant de poster sur le compte [Mastodon de Lily Bones](https://mamot.fr/@LilyBones) les morceaux en cours de diffusion sur [Real Rebel Radio](https://www.realrebelradio.net/).

> Born To Lose, Live To Win! \m/
